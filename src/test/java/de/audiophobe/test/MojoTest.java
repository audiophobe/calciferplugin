package de.audiophobe.test;

import de.audiophobe.CalciferMojo;
import de.audiophobe.parser.JavaSourceParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MojoTest {


  @Test
  public void testExtractJavadocStart() {
    JavaSourceParser sourceParser = new JavaSourceParser();
    Assertions.assertFalse(sourceParser.extractJavadocStart("/**"));
    Assertions.assertFalse(sourceParser.extractJavadocStart("   /**"));
    Assertions.assertFalse(sourceParser.extractJavadocStart("/*ab**"));
    Assertions.assertFalse(sourceParser.extractJavadocStart("/***Test123"));
    Assertions.assertFalse(sourceParser.extractJavadocStart("***"));
    Assertions.assertFalse(sourceParser.extractJavadocStart("/  ***"));

    Assertions.assertTrue(sourceParser.extractJavadocStart("/***"));
    Assertions.assertTrue(sourceParser.extractJavadocStart("   /***"));
    Assertions.assertTrue(sourceParser.extractJavadocStart("   /***   "));
    Assertions.assertTrue(sourceParser.extractJavadocStart("/***   "));
  }

  @Test
  public void testExtractJavadocEnde() {
    JavaSourceParser sourceParser = new JavaSourceParser();
    Assertions.assertFalse(sourceParser.extractJavadocEnde("//*"));
    Assertions.assertFalse(sourceParser.extractJavadocEnde("* /"));
    Assertions.assertFalse(sourceParser.extractJavadocEnde("ab*/"));
    Assertions.assertFalse(sourceParser.extractJavadocEnde("*/cd"));

    Assertions.assertTrue(sourceParser.extractJavadocEnde("*/"));
    Assertions.assertTrue(sourceParser.extractJavadocEnde("   */"));
    Assertions.assertTrue(sourceParser.extractJavadocEnde("   */   "));
    Assertions.assertTrue(sourceParser.extractJavadocEnde("*/   "));
  }




}
