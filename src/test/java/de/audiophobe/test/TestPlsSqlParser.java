package de.audiophobe.test;

import de.audiophobe.parser.PlSqlSourceParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestPlsSqlParser {

  @Test
  public void testParser() {
    PlSqlSourceParser plSqlSourceParser = new PlSqlSourceParser();

    Assertions.assertEquals(null,plSqlSourceParser.extractCalciferExp("ABC"));
    Assertions.assertEquals(null,plSqlSourceParser.extractCalciferExp("  --ABC"));
    Assertions.assertEquals("ABC",plSqlSourceParser.extractCalciferExp("  ---ABC"));

    Assertions.assertEquals(false,plSqlSourceParser.extractJavadocStart("  /*"));
    Assertions.assertEquals(false,plSqlSourceParser.extractJavadocStart("  /* **"));
    Assertions.assertEquals(true,plSqlSourceParser.extractJavadocStart("  /**"));
    Assertions.assertEquals(true,plSqlSourceParser.extractJavadocStart("  /** "));
    Assertions.assertEquals(true,plSqlSourceParser.extractJavadocStart("  /** blah bla"));
  }

}
