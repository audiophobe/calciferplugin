package de.audiophobe;

import java.util.ArrayList;
import java.util.List;

/***
 * p=Fields
 *
 * Fields are static members of java classes, gathered by using reflection to parse these classes.
 *
 * Check out de/audiophobe/test/TestConstant.java within this project
 *
 * Example:
 * ....
 * public final static String TEST2 = "Hello World 2";
 * ....
 *
 * TEST2 can be referenced in your documentation by
 *
 * * $ {TEST2} - If multiple classe expose a TEST2 the it is not defined, which one will be used
 * * $ {TestConstant.TEST2} - Same as above, but rather unlikely
 * * $ {de.audiophobe.test.TestConstant.TEST2}
 *
 * The value is: "${de.audiophobe.test.TestConstant.TEST2}"
 *
 * Same for list of values:
 *
 * ALL_VALUES = ${ALL_VALUES}
 */
public class CalciferField extends CalciferAtom {

  private String content;

  public CalciferField(String id) {
    super();
    this.id = id;
  }

  @Override
  public List<String> getContents(boolean isFragment, CalciferProject calciferProject) {
    List<String> result = new ArrayList<>();
    result.add(content);
    return result;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
