package de.audiophobe;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class CalciferProject {

  public static final String TOP = "top";

  public File rootDir;
  public String title;

  public Map<String, CalciferPage> pages = new HashMap<>();
  public Map<String, CalciferBlock> blocks = new HashMap<>();
  public Map<String, CalciferDiagram> diagrams = new HashMap<>();
  public Map<String, CalciferEnum> enums = new HashMap<>();
  public Map<String, CalciferField> fields = new HashMap<>();

  public CalciferProject() {
    super();
    pages.put(TOP, new CalciferPage(TOP));
  }

  public CalciferPage getPage(String pageName) {
    pages.putIfAbsent(pageName, new CalciferPage(pageName));
    return pages.get(pageName);
  }

  public CalciferBlock getBlock(String name) {
    blocks.putIfAbsent(name, new CalciferBlock(name));
    return blocks.get(name);
  }

  public CalciferField getField(String id) {
    fields.putIfAbsent(id, new CalciferField(id));
    return fields.get(id);
  }



  public CalciferDiagram getDiagram(String name) {
    diagrams.putIfAbsent(name, new CalciferDiagram(name));
    CalciferDiagram calciferDiagram = diagrams.get(name);
    calciferDiagram.setName(name);
    return diagrams.get(name);
  }

  public CalciferEnum getEnum(String name) {
    enums.putIfAbsent(name, new CalciferEnum());
    CalciferEnum calciferEnum = enums.get(name);
    return calciferEnum;
  }

  public void addBlockToPage(String pageName, CalciferBlock block) {
    CalciferPage calciferPage = getPage(pageName);
    block.addIdIfNull();
    calciferPage.getParts().add(block);
    blocks.put(block.getId(),block);
  }

  public CalciferAtom findPart(String id) {
    if (blocks.containsKey(id)) {
      return blocks.get(id);
    }
    if (enums.containsKey(id)) {
      return enums.get(id);
    }
    if (diagrams.containsKey(id)) {
      return diagrams.get(id);
    }
    if (fields.containsKey(id)) {
      return fields.get(id);
    }

    return null;
  }


}
