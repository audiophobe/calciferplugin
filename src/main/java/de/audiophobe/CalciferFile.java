package de.audiophobe;

import de.audiophobe.enums.CalciferFileTypeE;
import java.io.File;

/**
 * Represents a file to be parsed
 */
public class CalciferFile {

  private File file;
  private String extension;
  private CalciferFileTypeE filetype;

  public CalciferFile(File file, String extension, CalciferFileTypeE filetype) {
    this.file = file;
    this.extension = extension;
    this.filetype = filetype;
  }

  public File getFile() {
    return file;
  }

  public void setFile(File file) {
    this.file = file;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public CalciferFileTypeE getFiletype() {
    return filetype;
  }

  public void setFiletype(CalciferFileTypeE filetype) {
    this.filetype = filetype;
  }
}
