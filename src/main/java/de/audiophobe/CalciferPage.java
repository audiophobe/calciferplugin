package de.audiophobe;

import java.util.ArrayList;
import java.util.List;

/**
 * A single page of the documentation. Corresponds to a SCIIDOC-File
 */
public class CalciferPage extends CalciferAtom {

  private List<CalciferAtom> parts = new ArrayList<>();
  private String pageName;
  private String filename;

  public CalciferPage(String pageName) {
    super();
    setPageName(pageName);
  }

  public List<CalciferAtom> getParts() {
    return parts;
  }

  public void setParts(List<CalciferAtom> parts) {
    this.parts = parts;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getPageName() {
    return pageName;
  }

  public void setPageName(String pageName) {
    this.pageName = pageName;

    if (pageName.equals(CalciferProject.TOP)) {
      filename = "top-level-page.adoc";
    } else {
      filename = pageName.replaceAll("[^a-zA-z0-9\\.]", "_") + ".adoc";
    }
  }

  public String getFilename() {
    return filename;
  }

  @Override
  public List<String> getContents(boolean isFragment, CalciferProject calciferProject) {
    List<String> result = new ArrayList<>();

    for (CalciferAtom part : parts) {
      List<String> partLines = part.getContents(isFragment,calciferProject);
      result.addAll(partLines);
      result.add("");
    }
    return result;
  }
}
