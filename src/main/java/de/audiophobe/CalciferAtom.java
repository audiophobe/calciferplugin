package de.audiophobe;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/***
 * p=Atoms
 *
 * Atoms are the smalles part of your documentation. Every atom represents a piece of knowledge.
 *
 * Atoms can be blocks, codeblocks, diagrams, enums, ...
 *
 * Atoms can not be single lines.
 *
 * Every atom is and has to be built by using a directive (see there) and has an id. This id can then
 * be referenced by placeholders or the (i)include-directive.
 *
 * == Example
 *
 * ....
 *  ///b=myBlock
 *  ///Line1
 *  ///Line2
 *  ///Line3
 *  ....
 *
 *  forms the atom/block "myBlock"
 *
 *  You can reference it in other parts of your documentation by using
 *
 *  ///i=myBlock
 *
 *  or simply
 *
 *  $ {myBlock}
 */
public abstract class CalciferAtom {

  private final static AtomicInteger sequence = new AtomicInteger(100000);
  protected String id;

  public static String getNextBlockId() {
    return "b" + sequence.incrementAndGet();
  }

  public abstract List<String> getContents(boolean isFragment, CalciferProject calciferProject);

  public void addIdIfNull() {
    if (id == null) {
      id = getNextBlockId();
    }
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
