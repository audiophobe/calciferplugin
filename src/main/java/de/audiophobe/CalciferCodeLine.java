package de.audiophobe;

public class CalciferCodeLine {

  private String raw;
  private String parseRemoveSpace;
  private String parseWithSpace;
  private int index;

  public CalciferCodeLine(String raw, String parseRemoveSpace, String parseWithSpace, int index) {
    this.raw = raw;
    this.parseRemoveSpace = parseRemoveSpace;
    this.parseWithSpace = parseWithSpace;
    this.index = index;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public String getRaw() {
    return raw;
  }

  public void setRaw(String raw) {
    this.raw = raw;
  }

  public String getParseRemoveSpace() {
    return parseRemoveSpace;
  }

  public void setParseRemoveSpace(String parseRemoveSpace) {
    this.parseRemoveSpace = parseRemoveSpace;
  }

  public String getParseWithSpace() {
    return parseWithSpace;
  }

  public void setParseWithSpace(String parseWithSpace) {
    this.parseWithSpace = parseWithSpace;
  }
}
