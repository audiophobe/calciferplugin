package de.audiophobe.parser;

import de.audiophobe.CalciferCodeLine;
import de.audiophobe.CalciferUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.maven.plugin.logging.Log;

public class PlSqlSourceParser extends SouceParser {

  public boolean extractJavadocStart(String s) {
    if (s != null && !s.isEmpty()) {
      String regexp = " */\\*\\* *.*";
      Pattern p = Pattern.compile(regexp);
      Matcher m = p.matcher(s);
      if (m.matches()) {
        return true;
      }
    }
    return false;
  }

  /**
   *
   */

  public boolean extractJavadocEnde(String s) {
    if (s != null && !s.isEmpty()) {
      String regexp = " *\\*/ *";
      Pattern p = Pattern.compile(regexp);
      Matcher m = p.matcher(s);
      if (m.matches()) {
        return true;
      }
    }
    return false;
  }

  public String extractCalciferExp(String s) {
    if (s != null && !s.isEmpty()) {
      String regexp = ".*---(.*)";
      Pattern p = Pattern.compile(regexp);
      Matcher m = p.matcher(s);
      if (m.matches()) {
        return m.group(1);
      }
    }
    return null;
  }


  public List<CalciferCodeLine> extractLines(final File javaSource, final Log log) {

    List<CalciferCodeLine> allCodeLines = new ArrayList<>();

    try {
      List<String> lines = CalciferUtil.INSTANCE.readAllLines(javaSource, log);

      int i = 0;

      if (lines.size() > 0) {
        while (i < lines.size()) {
          String line = lines.get(i);
          String rawLine = lines.get(i);
          //log.info("Parse line " + line);

          boolean javaDocStart = extractJavadocStart(line);

          String lineWithSpace = null;

          if (javaDocStart) {
            //log.info("JavaDocStart: " + line);
            line = line.replaceFirst(" */\\*\\* *","").trim();
            allCodeLines.add(new CalciferCodeLine(rawLine, line, line, i));
            i++;
            line = lines.get(i);
            //getLog().info("Parse JavaDocLine " + line);
            while (!extractJavadocEnde(line)) {
              lineWithSpace = line;
              line = line.trim();
              //log.info("Add JavaDocLine " + line);
              allCodeLines.add(new CalciferCodeLine(rawLine, line, line, i));

              i++;
              line = lines.get(i);
            }
          } else {
            String x = extractCalciferExp(line);

            if (x != null) {
              //log.info("Add CLine line: " + x);
              allCodeLines.add(new CalciferCodeLine(rawLine, x, x, i));
            } else {
              allCodeLines.add(new CalciferCodeLine(rawLine, null, null, i));
            }
          }

          i++;
        }
      }


    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return allCodeLines;
  }
}
