package de.audiophobe.parser;

import de.audiophobe.CalciferCodeLine;
import java.io.File;
import java.util.List;
import org.apache.maven.plugin.logging.Log;

/**
 * A Sourceparser works on a single, text-based file.
 */
public abstract class SouceParser extends Parser {

  /**
   * Scan a file, and extract all relevant passages
   * @param javaSource The source file
   * @param log The Mojo-Logger
   * @return
   */
  public abstract List<CalciferCodeLine> extractLines(final File javaSource, final Log log);


}
