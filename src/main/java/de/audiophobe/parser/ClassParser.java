package de.audiophobe.parser;

import de.audiophobe.CalciferProject;
import java.io.File;
import java.net.URLClassLoader;
import org.apache.maven.plugin.logging.Log;

public abstract  class ClassParser extends Parser {

  public abstract  void parseClass(final File classFile, CalciferProject calciferProject, Log log, ClassLoader classLoader);

}
