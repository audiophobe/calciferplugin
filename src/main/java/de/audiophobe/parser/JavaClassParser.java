package de.audiophobe.parser;

import de.audiophobe.CalciferProject;
import de.audiophobe.CalciferUtil;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.maven.plugin.logging.Log;

public class JavaClassParser extends ClassParser {

  public void parseClass(final File classFile, CalciferProject calciferProject, Log log, ClassLoader classLoader) {
    try {
      log.info("Parse Java " + classFile.getAbsolutePath());

      List<String> lines = CalciferUtil.INSTANCE.readAllLines(classFile,log);

      String packageName = lines.stream().filter(x -> x.startsWith("package"))
          .map(x -> x.replaceFirst("package +", "").replaceAll(" *;.*", "")).findFirst().get();
      String className = classFile.getName().replace(".java", "");
      String canonicalName = packageName + "." + className;
      log.info("Parse package " + canonicalName);
      Class clazz = classLoader.loadClass(canonicalName);
      log.info("Class loaded");

      if (clazz.isEnum()) {
        log.info("is Enum");
        Object[] enumC = clazz.getEnumConstants();

        StringBuilder allEnumHeader = new StringBuilder();
        StringBuilder allEnumRows = new StringBuilder();
        allEnumRows.append("== " + canonicalName + "\n");

        List<Field> relevantFields = Arrays.stream(clazz.getDeclaredFields()).filter(x -> !x.isSynthetic() && !x.isEnumConstant()).collect(
            Collectors.toList());

        relevantFields = relevantFields.stream().filter(x -> Modifier.isPrivate(x.getModifiers())).collect(Collectors.toList());

        relevantFields.forEach(x -> x.setAccessible(true));

        for (Field f : relevantFields) {
          log.info("Field " + f.getName() + " Mod " + f.getModifiers() + " " + Modifier.toString(f.getModifiers()));
        }

        List<String> fieldnames = relevantFields.stream().map(x -> x.getName()).collect(Collectors.toList());

        log.info("Relevant fields: " + fieldnames.stream().collect(Collectors.joining(", ")));

        allEnumHeader.append("|===\n");
        allEnumHeader.append("|name");
        fieldnames.forEach(x -> allEnumHeader.append("|" + x));
        allEnumHeader.append("\n");
        allEnumRows.append(allEnumHeader.toString());

        if (enumC != null && enumC.length > 0) {
          log.info("Enum with " + enumC.length + " entries");
          for (Object o : enumC) {
            log.info("Enum Object " + o);

            String singleEnumRow = "";

            singleEnumRow = singleEnumRow + "|" + o.toString();

            for (Field field : relevantFields) {
              log.info("Enum Field: " + field.getName() + ", Type: " + field.getGenericType());

              Object valO = field.get(o);
              singleEnumRow = singleEnumRow + "|";

              if (valO != null) {
                singleEnumRow = singleEnumRow + valO.toString();
              }
            }
            String canonicalFieldName = canonicalName + "." + o.toString();
            String canonicalNoclassFieldName = className + "." + o.toString();
            singleEnumRow = singleEnumRow + "\n";
            allEnumRows.append(singleEnumRow + "\n");
            singleEnumRow = allEnumHeader + singleEnumRow + "|===\n";

            calciferProject.getField(canonicalFieldName).setContent(singleEnumRow.toString());
            log.info("Put in " + canonicalFieldName + " => " + singleEnumRow.toString());
            calciferProject.getField(canonicalNoclassFieldName).setContent(singleEnumRow.toString());
            log.info("Put in " + canonicalNoclassFieldName + " => " + singleEnumRow.toString());
          }
        }

        allEnumRows.append("|===\n");
        log.info("Put in " + canonicalName + " => " + allEnumRows);
        calciferProject.getField(canonicalName).setContent(allEnumRows.toString());

        log.info("Put in " + className + " => " + allEnumRows);
        calciferProject.getField(className).setContent(allEnumRows.toString());
      }

      if (!clazz.isEnum()) {
        log.info("Class is no enum");
        for (Field field : clazz.getDeclaredFields()) {
          String fieldname = field.getName();
          log.info("Found field " + fieldname);

          String canonicalFieldName = packageName + "." + className + "." + fieldname;
          String canonicalFieldNameNoPac = className + "." + fieldname;

          if (!field.isSynthetic()) {
            field.setAccessible(true);
            log.info("Field active: " + field.getName() + ", Type: " + field.getGenericType());

            try {
              Object o = field.get(null);
              if (o != null) {
                if (o instanceof List) {
                  List oList = (List) o;
                  String oVal = "";
                  for (Object o2 : oList) {
                    oVal = oVal + ", " + o2.toString();
                  }
                  oVal = oVal.replaceFirst("..", "");
                  calciferProject.getField(fieldname).setContent(oVal);
                  calciferProject.getField(canonicalFieldNameNoPac).setContent(oVal);
                  calciferProject.getField(canonicalFieldName).setContent(oVal);
                  log.info("Field " + canonicalFieldName + " => " + oVal);
                } else {
                  String oVal = o.toString();
                  calciferProject.getField(fieldname).setContent(oVal);
                  calciferProject.getField(canonicalFieldNameNoPac).setContent(oVal);
                  calciferProject.getField(canonicalFieldName).setContent(oVal);
                  log.info("Field " + canonicalFieldName + " => " + oVal);
                }
              }
            } catch (Exception e) {
              log.info("Ignore " + canonicalFieldName + " for " + e.getMessage());
            }
          }
        }
      }
    } catch (Throwable e) {
      log.error("Ignored File " + classFile.getAbsolutePath() + " for " + e.getMessage(), e);
    }
  }


}
