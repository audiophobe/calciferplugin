package de.audiophobe.parser;

import de.audiophobe.CalciferCodeLine;
import de.audiophobe.CalciferUtil;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.maven.plugin.logging.Log;

public class JavaSourceParser extends SouceParser {

  public boolean extractJavadocStart(String s) {
    if (s != null && !s.isEmpty()) {
      String regexp = " */\\*\\*\\* *";
      Pattern p = Pattern.compile(regexp);
      Matcher m = p.matcher(s);
      if (m.matches()) {
        return true;
      }
    }
    return false;
  }

  /**
   *
   */

  public boolean extractJavadocEnde(String s) {
    if (s != null && !s.isEmpty()) {
      String regexp = " *\\*/ *";
      Pattern p = Pattern.compile(regexp);
      Matcher m = p.matcher(s);
      if (m.matches()) {
        return true;
      }
    }
    return false;
  }

  public String extractCalciferExp(String s) {
    if (s != null && !s.isEmpty()) {
      String regexp = ".*/" + "/" + "/(.*)";
      Pattern p = Pattern.compile(regexp);
      Matcher m = p.matcher(s);
      if (m.matches()) {
        return m.group(1);
      }
    }
    return null;
  }


  public List<CalciferCodeLine> extractLines(final File javaSource, final Log log) {

    List<CalciferCodeLine> allCodeLines = new ArrayList<>();

    try {
      List<String> lines = CalciferUtil.INSTANCE.readAllLines(javaSource,log);
      List<String> allComments = new ArrayList<>();

      int i = 0;

      if (lines.size() > 0) {
        while (i < lines.size()) {
          String line = lines.get(i);
          String rawLine = lines.get(i);
          //getLog().info("Parse line " + line);

          boolean javaDocStart = extractJavadocStart(line);

          if (javaDocStart) {
            //getLog().info("JavaDocStart");
            i++;
            line = lines.get(i);
            //getLog().info("Parse JavaDocLine " + line);
            while (!extractJavadocEnde(line)) {
              if (!line.matches(".*(@param|@return|@throws).*")) {
                String lineWithSpace = line.replaceFirst(" *\\* ", "");
                line = line.replaceFirst(" *\\* *", "");
                //getLog().info("Add JavaDocLine " + line);
                allComments.add(line);
                allCodeLines.add(new CalciferCodeLine(rawLine, line, lineWithSpace, i));
              }

              i++;
              line = lines.get(i);
            }
          } else {
            String x = extractCalciferExp(line);

            if (x != null) {
              //getLog().info("Add regular line: " + x);
              allComments.add(x);
              allCodeLines.add(new CalciferCodeLine(rawLine, x, x, i));
            } else {
              allComments.add(null);
              allCodeLines.add(new CalciferCodeLine(rawLine, null, null, i));
            }
          }

          i++;
        }
      }


    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return allCodeLines;
  }
}
