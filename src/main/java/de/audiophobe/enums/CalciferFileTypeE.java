package de.audiophobe.enums;

public enum CalciferFileTypeE {

  JAVA_SOURCE, JAVA_CLASS, SQL;

}
