package de.audiophobe;

import java.util.ArrayList;
import java.util.List;

/**
 * A Diagram
 */
public class CalciferDiagram extends CalciferAtom {

  public static final String IMPORT_DIAGRAM = "IMPORT_DIAGRAM=";

  private List<String> entries = new ArrayList<>();
  private String name;

  public CalciferDiagram(String name) {
    this.name = name;
  }

  public void add(String s) {
    entries.add(s);
  }

  public List<String> getContents(boolean isFragment, CalciferProject calciferProject) {
    List<String> result = new ArrayList<>();

    if (!isFragment) {
      result.add("[plantuml, " + name + ", png]");
      result.add("....");
    }

    for (String s : entries) {
      if (s.startsWith("id=")) {
        String includeDiagramName = s.replace("id=", "");
        result.add(CalciferMojo.INCLUDE_FRAGMENT + includeDiagramName);
      } else {
        result.add(s);
      }
    }

    if (!isFragment) {
      result.add("....");
      result.add("");
    }

    if (!isFragment) {
      result.add("");
    }

    return result;
  }

  public List<String> getEntries() {
    return entries;
  }

  public void setEntries(List<String> entries) {
    this.entries = entries;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
