package de.audiophobe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/***
 * p=Java-Enums
 *
 * All java enumerations are automatically analysed by reflection. Every Enum will become an Atom, as
 * will any of its values.
 *
 * Check out de/audiophobe/test/TestEnumE.java within this project
 *
 * It can be referenced by the canonical name
 *
 * * => i=de.audiophobe.test.TestEnumE
 * * => ${de.audiophobe.test.TestEnumE}
 *
 *
 * i=de.audiophobe.test.TestEnumE
 *
 *
 * or simply
 *
 * * => i=TestEnumE
 * * => $ {TestEnumE}
 *
 * i=TestEnumE
 *
 * as with its fields:
 *
 * => i=de.audiophobe.test.TestEnumE.ENUM_ENTRY_1
 *
 * i=de.audiophobe.test.TestEnumE.ENUM_ENTRY_1
 *
 * or
 *
 * => i=TestEnumE.ENUM_ENTRY_1
 *
 * i=TestEnumE.ENUM_ENTRY_1
 */




public class CalciferEnum extends CalciferAtom {

  private String classname;
  private String classnameTranslated;
  private List<String> fields = new ArrayList<>();
  private ClassLoader classLoader;

  public Object getValueIncludingSuperclasses(String fieldName, Object o) {
    try {

      Field field = null;
      try {
        field = o.getClass().getDeclaredField(fieldName);
      } catch (NoSuchFieldException e) {
        return getValueIncludingSuperclasses(fieldName,o.getClass().getSuperclass());
      }
      field.setAccessible(true);
      Object val = field.get(o);
      return val;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public List<String> getContents(boolean isFragment, CalciferProject calciferProject) {
    List<String> result = new ArrayList<>();

    result.add("== " + classname);

    String header = "[cols=\"";
    header = header + fields.stream().collect(Collectors.joining(","));
    header = header + "\"]";
    result.add(header);
    result.add("|===");

    header = "|" + fields.stream().collect(Collectors.joining("|"));
    result.add(header);

    try {
      Class c = classLoader.loadClass(classname);
      Object[] enumConst = c.getEnumConstants();

      for (Object o : enumConst) {
        result.add("");

        String row = "";
        for (String field : fields) {
          Object val = getValueIncludingSuperclasses(field, o);

          if (val == null) {
            row = row + "|";
          } else if (val instanceof String) {
            row = row + "|" + (String) val;
          }
        }
        result.add(row);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    result.add("|===");
    return result;
  }

  public String getClassname() {
    return classname;
  }

  public void setClassname(String classname) {
    this.classname = classname;
    classnameTranslated = classname.replace(".","\\") + ".class";
  }

  public List<String> getFields() {
    return fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  public String getClassnameTranslated() {
    return classnameTranslated;
  }

  public void setClassnameTranslated(String classnameTranslated) {
    this.classnameTranslated = classnameTranslated;
  }

  public ClassLoader getClassLoader() {
    return classLoader;
  }

  public void setClassLoader(ClassLoader classLoader) {
    this.classLoader = classLoader;
  }
}

