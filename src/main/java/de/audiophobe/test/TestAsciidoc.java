package de.audiophobe.test;

public class TestAsciidoc {
  ///p=Working with ASCIIDOC

  ///This is just some text..
  ///Multiple lines are concatenated.
  ///Linefeeds may be ignored by asciidoc.

  ///.This is a list
  ///* Just some line
  ///* Just some line
  ///* Just some line

  ///---

  ///.This is a _second_ list
  ///. This may be important
  ///. This may be important
  ///. This may be important

  ///---

  ///.This is a checklist
  ///* [] Nope
  ///* [x]. Yay
  ///* [x] Oki

  ///---

  ///.This is a complex List
  ///* This is an entry
  ///This too
  ///
  ///* This is an entry
  ///This too
  ///
  ///* This is an entry
  ///This too

  ///---


}
