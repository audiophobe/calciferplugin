package de.audiophobe.test;

public class TestEnum {
  ///p=Working with Enums


  ///== de.audiophobe.test.TestEnumE.ENUM_ENTRY_1
  ///i=de.audiophobe.test.TestEnumE.ENUM_ENTRY_1

  ///== de.audiophobe.test.TestEnumE.ENUM_ENTRY_2
  ///i=de.audiophobe.test.TestEnumE.ENUM_ENTRY_2


  ///== Whole enum, full path
  ///i=de.audiophobe.test.TestEnumE


  ///== Whole enum, classname only
  ///i=TestEnumE

  ///== Whole enum, placeholder
  ///{TestEnumE}




}
