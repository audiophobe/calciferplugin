package de.audiophobe.test;

public class TestDiagram {
  ///p=Working with diagrams

  ///d=myDiagramId;Title States
  ///dl=state "State A" as A
  ///dl=[*] --> A
  ///dl=A --> B : Action
  ///dl=state "State B" as B
  ///dl=B --> C : Action
  ///dl=state "State C" as C
  ///dl=C --> D : Action
  ///dl=C --> E : Action
  ///dl=C --> F : Action
  ///dl=state "State D" as D
  ///dl=D --> G : Action
  ///dl=state "State E" as E
  ///dl=E --> D : Action
  ///dl=state "State F" as F
  ///dl=F --> F : Action
  ///dl=state "State G" as G
  ///dl=F --> G : Action
  ///dl=G --> [*]

  ///Let's include the diagram

  ///id=myDiagramId


  ///Test mit Fragments

  ///d=TestFragment1;Title Fragment1
  ///dl=[*] --> A
  ///dl=A --> B
  ///dl=B --> [*]

  ///d=TestFragment2;Title Fragment2
  ///dl=[*] --> X
  ///dl=X --> Y
  ///dl=Y --> [*]

  ///d=TestFragment3;Title Fragment Test
  ///dl=State EXT_A {
  ///dl=id=TestFragment1
  ///dl=}
  ///dl=State EXT_B {
  ///dl=id=TestFragment2
  ///dl=}
  ///dl=[*] --> EXT_A
  ///dl=EXT_A --> EXT_B
  ///dl=EXT_B --> [*]

  ///Fragement 1

  ///i=TestFragment1

  ///Fragement 2

  ///i=TestFragment2

  ///Fragement 3

  ///i=TestFragment3



}
