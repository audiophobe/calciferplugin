package de.audiophobe.test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TestConstant {
  ///p=Working with fields

  public final static String TEST2 = "Hello World 2";
  private final static String TEST1 = "Hello World 1";
  private final static Date NOW = new Date();
  private final static TestConstant INSTANT = new TestConstant();
  public static String TEST4 = "Hello World 4";
  private static String TEST3 = "Hello World 3";

  public final static List<String > ALL_VALUES = Arrays.asList(new String[]{"One","Two","Three"});

  /***
   * * TEST1 = ${TEST1}
   * * TEST2 = ${TEST2}
   * * TEST3 = ${TEST3}
   * * TEST4 = ${TEST4}
   * * ALL_VALUES = ${ALL_VALUES}
   */

  /***
   * * TEST1 = ${TestConstant.TEST1}
   * * TEST2 = ${TestConstant.TEST2}
   * * TEST3 = ${TestConstant.TEST3}
   * * TEST4 = ${TestConstant.TEST4}
   * * ALL_VALUES = ${TestConstant.ALL_VALUES}
   */

}
