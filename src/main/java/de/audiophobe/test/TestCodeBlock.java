package de.audiophobe.test;

public class TestCodeBlock {
  ///p=Working with codeblocks

  /***
   * cb=myCodeblockId
   * {
   *   "aeNummer" : 722617,
   *   "neNummer" : 490,
   *   "geraetetypen" : [{
   *       "geraetetyp" : "KWZ",
   *       "fehlercodes" : [{
   *           "_id" : "MIETVER",
   *           "text" : "AUST: Gerät verbaut",
   *           "adNutzeinheitId" : 179908256,
   *           "lfdNr" : "B00100",
   *           "raum" : "Bad"
   *         }],
   *       "einbauErfolgreich" : false,
   *       "einbauErfolgreichLfdNummer" : []
   *     }, {
   *       "geraetetyp" : "HKVE",
   *       "fehlercodes" : [{
   *           "_id" : "MIETVER",
   *           "text" : "HK verbaut",
   *           "adNutzeinheitId" : 179908256,
   *           "lfdNr" : "A00200",
   *           "raum" : "Küche"
   *         }],
   *       "einbauErfolgreich" : true,
   *       "einbauErfolgreichLfdNummer" : ["A00400", "A00300", "A00100"]
   *     }],
   *   "status" : "FERT",
   *   "lage" : "EG",
   *   "bearbeitungsdatum" : ISODate("2022-02-11T08:39:37Z"),
   *   "errorsForOperations" : [],
   *   "errorsForTs" : [],
   *   "bearbeitet" : true,
   *   "timestampFirstStatusFound" : ISODate("2022-02-16T21:32:39.989Z"),
   *   "version" : 1,
   *   "erzeugungsdatum" : ISODate("2022-02-15T23:00:00Z")
   * }
   */

  ///Let's include the codeblock

  ///i=myCodeblockId

}
