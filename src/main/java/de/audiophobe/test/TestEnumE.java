package de.audiophobe.test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public enum TestEnumE {

  ENUM_ENTRY_1("Test1", 111, new Date()), ENUM_ENTRY_2("Test2", 222, new Date()), ENUM_ENTRY_3("Test3", 333, new Date());

  public static final List<TestEnumE> ALL_VALUES = Arrays.asList(ENUM_ENTRY_1, ENUM_ENTRY_2, ENUM_ENTRY_3);

  private String testString;
  private Integer testNumber;
  private Date testDate;

  TestEnumE(String testString, Integer testNumber, Date testDate) {
    this.testString = testString;
    this.testNumber = testNumber;
    this.testDate = testDate;
  }
}
