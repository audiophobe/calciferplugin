package de.audiophobe.test;

public class TestBlockDirective {

  ///p=Working with blocks

  /***
   * == Test ignore javadoc parameters
   * Only this line should print
   * @param s
   * @param n
   * @return
   */
  public String test(String s, Number n) {
    return "Hallo";
  }

  /***
   * b=Testblock1
   * == Testblock 1
   * This belongs to testblock 1. This belongs to testblock 1. This belongs to testblock 1.
   * This belongs to testblock 1. This belongs to testblock 1. This belongs to testblock 1.
   * This belongs to testblock 1. This belongs to testblock 1. This belongs to testblock 1.
   */



  /***
   * b=Testblock2
   * == Testblock 2
   * This belongs to testblock 2. This belongs to testblock 2. This belongs to testblock 2.

   * This belongs to testblock 2. This belongs to testblock 2. This belongs to testblock 2.

   * This belongs to testblock 2. This belongs to testblock 2. This belongs to testblock 2.
   */

  /***
   * blah!
   */

  /***
   * b=Testblock2
   * This belongs to testblock 2. This belongs to testblock 2. This belongs to testblock 2.

   * This belongs to testblock 2. This belongs to testblock 2. This belongs to testblock 2.

   * This belongs to testblock 2. This belongs to testblock 2. This belongs to testblock 2.
   */


  ///Let's include the first block

  ///i=Testblock1

  ///Let's include the second block. You'll see that both parts have been concatenated

  ///i=Testblock2





}
