package de.audiophobe;

import de.audiophobe.enums.CalciferFileTypeE;
import de.audiophobe.parser.JavaClassParser;
import de.audiophobe.parser.JavaSourceParser;
import de.audiophobe.parser.PlSqlSourceParser;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;


///p=CalciferMojo
@Mojo(name = "parse", requiresDependencyResolution = ResolutionScope.TEST)
public class CalciferMojo extends AbstractMojo {

  public static final String INCLUDE = "TIP: IMPORT HERE ";
  public static final String INCLUDE_FRAGMENT = "TIP: IMPORT FRAGMENT HERE ";
  public static final List<String> ALLOWED_FILETYPES = Arrays.asList(".class", ".java", ".sql");
  public static List<String> ENCODINGS = Arrays.asList(new String[]{"UTF-8", "ISO-8859-1", "windows-1250"});
  @Parameter(defaultValue = "${project}", readonly = true, required = true)
  private MavenProject project;
  @Parameter(defaultValue = "${session}", readonly = true, required = true)
  private MavenSession session;

  private final static  CodeExtractor codeExtractor = new CodeExtractor();

  /***
   * p=Configuration
   * The Calcifer-Plugin will always recursively scan the project. Additional
   * directories can be included via the plugin configuration in the
   * pom.xml
   * ....
   * <configuration>
   *   <codeDirectories>
   *     <param>C:\Dir1</param>
   *     <param>C:\Dir2</param>
   *     <param>C:\Dir3</param>
   *   </codeDirectories>
   * </configuration>
   * ....
   */
  @Parameter
  private List<File> codeDirectories = new ArrayList<>();

  private ClassLoader extendedLoader = null;

  private List<CalciferFile> allFiles = new ArrayList<>();

  public boolean isValidFile(File f) {
    if (f.isFile() && f.exists() && f.length() > 0) {
      for (String s : ALLOWED_FILETYPES) {
        if (f.getName().endsWith(s)) {
          return true;
        }
      }
    }
    return false;
  }

  public boolean isValidDirectory(File f) {
    return f.isDirectory() && !f.getName().startsWith(".git") && !f.getName().startsWith("target") && !f.getName().startsWith(".idea");

  }

  public void listFiles(File root) {
    getLog().info("Traverse " + root.getAbsolutePath());
    List<File> workDir = Arrays.asList(root.listFiles());
    getLog().info("Found: " + workDir.size());

    for (File f : workDir) {
      getLog().info("Work on " + f.getAbsolutePath());
      if (isValidFile(f)) {
        String extension = f.getName().replaceAll("^.*\\.", "");
        CalciferFileTypeE filetype = null;

        if (extension != null) {
          switch (extension) {
            case "java":
              filetype = CalciferFileTypeE.JAVA_SOURCE;
              break;
            case "class":
              filetype = CalciferFileTypeE.JAVA_CLASS;
              break;
            case "sql":
              filetype = CalciferFileTypeE.SQL;
              break;
            default:
              getLog().error("Unknown Type of File: " + f.getAbsolutePath());
          }
        }

        if (extension != null) {
          CalciferFile calciferFile = new CalciferFile(f, extension, filetype);
          allFiles.add(calciferFile);
          getLog().info("Added " + f.getAbsolutePath());
        }
      } else if (isValidDirectory(f)) {
        listFiles(f);
      }
    }
  }

  public void execute() throws MojoExecutionException {
    try {
      getLog().info("Running in Basedir " + project.getBasedir().getAbsolutePath());
      addClasspath();
      listFiles(project.getBasedir());

      getLog().info("Traversing codeDirectories: " + codeDirectories.size());
      if (codeDirectories != null) {
        for (File dir : codeDirectories) {
          if (!dir.exists()) {
            getLog().error("Directory does not exist: " + dir.getAbsolutePath());
          } else if (!dir.isDirectory()) {
            getLog().error("Not a directory: " + dir.getAbsolutePath());
          } else {
            listFiles(dir);
          }
        }
      }

      createMarkup();
    } catch (Exception e) {
      e.printStackTrace();
      getLog().error("Error in execution", e);
      throw new MojoExecutionException("Error execution", e);
    }
  }


  public void createMarkupForFile(CalciferFile f, CalciferProject calciferProject) {
    getLog().info("Create markup for " + f.getFile().getAbsolutePath());

    try {
      List<CalciferCodeLine> allCodeLines = null;

      if (f.getFiletype().equals(CalciferFileTypeE.JAVA_SOURCE)) {
        JavaSourceParser javaSourceParser = new JavaSourceParser();
        allCodeLines = javaSourceParser.extractLines(f.getFile(), getLog());
      }

      if (f.getFiletype().equals(CalciferFileTypeE.SQL)) {
        PlSqlSourceParser plSqlSourceParser = new PlSqlSourceParser();
        allCodeLines = plSqlSourceParser.extractLines(f.getFile(), getLog());
      }

      CalciferCodeStateE state = CalciferCodeStateE.NONE;

      CalciferBlock currentBlock = new CalciferBlock();

      String currentPage = CalciferProject.TOP;
      String currentBlockMarker = null;
      String currentDiagramName = null;

      for (CalciferCodeLine cl : allCodeLines) {
        String c = cl.getParseRemoveSpace();
        if (c != null) {
          String page = codeExtractor.extractPage(c);
          String block = codeExtractor.extractBlockMarker(c);
          String codeblock = codeExtractor.extractCodeBlockMarker(c);
          String includeMarker =codeExtractor. extractIncludeMarker(c);
          DiagramLine diagramLine = codeExtractor.extractDiagramLine(c);
          EnumLine enumLine = codeExtractor.extractEnumLine(c);

          if (enumLine != null) {
            CalciferEnum calciferEnum = calciferProject.getEnum(enumLine.enumName);
            calciferEnum.setClassname(enumLine.enumName);
            calciferEnum.setFields(Arrays.asList(enumLine.fields.split(",")));
            calciferEnum.setClassLoader(extendedLoader);
          } else if (includeMarker != null) {
            currentBlock.add(INCLUDE + includeMarker);
          } else if (diagramLine != null) {
            if (diagramLine.diagramName != null) {
              currentDiagramName = diagramLine.diagramName;
            } else {
              diagramLine.diagramName = currentDiagramName;
            }
            CalciferDiagram calciferDiagram = calciferProject.getDiagram(currentDiagramName);
            calciferDiagram.add(diagramLine.line);
            getLog().info("Diagram: " + currentDiagramName + ", line: " + diagramLine.line);
          } else if (block != null) {
            state = CalciferCodeStateE.BLOCK;
            currentBlockMarker = block;
            currentBlock = calciferProject.getBlock(currentBlockMarker);
          } else if (codeblock != null) {
            state = CalciferCodeStateE.CODEBLOCK;
            currentBlockMarker = codeblock;
            currentBlock = calciferProject.getBlock(currentBlockMarker);
            currentBlock.add("....");
          } else if (page != null) {
            currentPage = page;
            //getLog().info("New Page: " + page);
          } else {
            if (state == CalciferCodeStateE.CODEBLOCK) {
              currentBlock.add(cl.getParseWithSpace());
            } else {
              currentBlock.add(c);
            }
          }
        } else {
          if (!currentBlock.isEmpty()) {
            if (state == CalciferCodeStateE.CODEBLOCK) {
              currentBlock.add("....");
            }

            if (currentBlockMarker != null) {
              calciferProject.blocks.put(currentBlockMarker, currentBlock);
            } else {
              calciferProject.addBlockToPage(currentPage, currentBlock);
            }

            currentBlock = new CalciferBlock();
            state = CalciferCodeStateE.NONE;
            currentBlockMarker = null;
          }
        }
      }

      if (!currentBlock.isEmpty()) {
        calciferProject.addBlockToPage(currentPage, currentBlock);
      }
    } catch (Exception e) {
      getLog().error(e);
      throw new RuntimeException(e);
    }
    getLog().info("Done with " + f.getFile().getAbsolutePath());
  }

  public void writePage(File dir, String filename, String content) {
    dir.mkdirs();
    File f = new File(dir, filename);
    try {
      Files.write(f.toPath(), content.getBytes(StandardCharsets.UTF_8));
    } catch (Exception e) {
      getLog().error(e);
      throw new RuntimeException(e);
    }
  }

  public String replaceFields(final String s, final CalciferProject calciferProject) {
    String result = s;
    List<String> placeholders = codeExtractor.getPlaceholders(result,getLog());
    if (result != null && !placeholders.isEmpty()) {
      for (String ph : placeholders) {
        String toReplace = "${" + ph + "}";
        getLog().info("Check placeholder " + toReplace);
        CalciferAtom calciferAtom = calciferProject.findPart(ph);
        if (calciferAtom != null) {
          String replaceWith = calciferAtom.getContents(true, calciferProject).stream().collect(Collectors.joining("\n"));
          getLog().info("Replace " + toReplace + " with " + replaceWith);
          result = result.replace(toReplace, replaceWith);
        }
      }
    }
    return result;
  }


  public void generateMarkup(CalciferProject calciferProject) {
    for (String pageName : calciferProject.pages.keySet()) {
      getLog().info("Generate markup für pageName " + pageName);
      CalciferPage calciferPage = calciferProject.getPage(pageName);

      File dir = null;

      if (pageName.equals(CalciferProject.TOP)) {
        dir = calciferProject.rootDir;
      } else {
        dir = new File(calciferProject.rootDir, "top-level-page");
        dir.mkdirs();
      }

      String title = calciferPage.getPageName();
      String filename = calciferPage.getFilename();

      //getLog().info("Generate markup für pageName " + pageName + ", title: " + title + ", filename: " + filename);

      StringBuilder sb = new StringBuilder();

      sb.append("= " + title + "\n\n");

      List<String> allContent = calciferPage.getContents(false, calciferProject);

      while (true) {
        List<String> allContentCollected = new ArrayList<>();

        boolean include = false;

        for (String line : allContent) {
          if (line.startsWith(INCLUDE)) {
            String partId = line.replace(INCLUDE, "");
            getLog().info("Include " + partId);
            CalciferAtom part = calciferProject.findPart(partId);

            if (part == null) {
              throw new RuntimeException("Cannot find part " + partId);
            }

            List<String> partContent = part.getContents(false, calciferProject);
            allContentCollected.addAll(partContent);
            getLog().info("Added " + partContent.size() + " from " + partId);
            include = true;
          } else if (line.startsWith(INCLUDE_FRAGMENT)) {
            String partId = line.replace(INCLUDE_FRAGMENT, "");
            getLog().info("Include fragment " + partId);
            CalciferAtom part = calciferProject.findPart(partId);

            if (part == null) {
              throw new RuntimeException("Cannot find part " + partId);
            }

            List<String> partContent = part.getContents(true, calciferProject);
            allContentCollected.addAll(partContent);
            getLog().info("Added " + partContent.size() + " from " + partId);
            include = true;
          } else {
            String replacedFields = replaceFields(line, calciferProject);
            allContentCollected.add(replacedFields);
            if (!line.equals(replacedFields)) {
              include = true;
            }
          }
        }

        allContent = allContentCollected;

        if (include == false) {
          break;
        }
      }

      for (String line : allContent) {
        sb.append(line);
        sb.append("\n");
      }

      String content = sb.toString();
      writePage(dir, filename, content);
    }
  }

  boolean deleteDirectory(File directoryToBeDeleted) {
    File[] allContents = directoryToBeDeleted.listFiles();
    if (allContents != null) {
      for (File file : allContents) {
        deleteDirectory(file);
      }
    }
    return directoryToBeDeleted.delete();
  }

  public void createMarkup() {
    StringBuilder sb = new StringBuilder();
    File rootDir = new File(project.getBasedir(), "/confluence");

    if (rootDir.exists()) {
      try {
        deleteDirectory(rootDir);
      } catch (Exception e) {
        getLog().error(e);
        throw new RuntimeException(e);
      }
    }

    sb.append("= Generierte Doku zu " + project.getName() + "\n");

    CalciferProject calciferProject = new CalciferProject();
    calciferProject.rootDir = rootDir;
    calciferProject.title = project.getName();

    JavaClassParser classParser = new JavaClassParser();

    for (CalciferFile f : allFiles) {
      if (f.getFiletype().equals(CalciferFileTypeE.JAVA_SOURCE)) {
        createMarkupForFile(f, calciferProject);
        classParser.parseClass(f.getFile(), calciferProject, getLog(), extendedLoader);
      }
    }

    for (CalciferFile f : allFiles) {
      if (f.getFiletype().equals(CalciferFileTypeE.SQL)) {
        createMarkupForFile(f, calciferProject);
      }
    }

    generateMarkup(calciferProject);
  }

  public void addClasspath() {
    try {
      Set<URL> urls = new HashSet<>();
      Set<String> elements = new HashSet<>();

      elements.addAll(project.getCompileClasspathElements());
      elements.addAll(project.getRuntimeClasspathElements());
      elements.addAll(project.getTestClasspathElements());

      getLog().info("Check deps");
      for (Dependency dependency : project.getDependencies()) {
        getLog().info(dependency.getSystemPath());
        getLog().info("A:" + dependency.getArtifactId());
      }

      for (String element : elements) {
        getLog().info("Add Classpath Element: " + element);
        urls.add(new File(element).toURI().toURL());
      }

      extendedLoader = URLClassLoader.newInstance(
          urls.toArray(new URL[0]),
          Thread.currentThread().getContextClassLoader());
    } catch (Exception e) {
      getLog().error(e);
      throw new RuntimeException(e);
    }
  }


}
