package de.audiophobe;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import org.apache.maven.plugin.logging.Log;

public class CalciferUtil {

  public static final CalciferUtil INSTANCE = new CalciferUtil();

  public static List<String> ENCODINGS = Arrays.asList(new String[]{"UTF-8", "ISO-8859-1", "windows-1250"});

  public List<String> readAllLines(final File f, final Log log) {
    List<String> lines = null;

    for (String encoding : ENCODINGS) {
      log.info("Try " + f.getAbsolutePath() + " with encoding " + encoding);
      try {
        lines = Files.readAllLines(f.toPath(), Charset.forName(encoding));
        break;
      } catch (final Throwable e) {
        log.debug("Nope");
      }
    }

    if (lines == null) {
      throw new RuntimeException("No valid encoding found for " + f.getAbsolutePath());
    } else {
      return lines;
    }


  }


}
