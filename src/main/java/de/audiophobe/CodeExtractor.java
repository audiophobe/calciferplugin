package de.audiophobe;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.maven.plugin.logging.Log;

public class CodeExtractor {
  ///p=Directives

  /***
   * == The p directive (Page)
   *
   * Syntax: p=Page Title
   *
   * All comments following this directive will be added to the ASCIIDOC-Page "Page Title"
   *
   * @param s
   * @return
   */
  public String extractPage(String s) {
    String regex = "p=(.*)";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(s);
    if (matcher.matches()) {
      return matcher.group(1);
    }
    return null;
  }

  /***
   * == The b directive (Block)
   *
   * Syntax: b=Block Name
   *
   * All following lines will be added to the block "Block Name", which can be referenced later
   * @param s
   * @return
   */
  public String extractBlockMarker(String s) {
    String regex = "b=(.*)";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(s);
    if (matcher.matches()) {
      return matcher.group(1);
    }
    return null;
  }

  /***
   * == The cb directive (Codeblock)
   *
   * Syntax: cb=Block Name
   *
   * All following lines will be added to the codeblock "Block Name", which can be referenced later.
   * A codeblock is preceeded and terminated by "....". ASCIIDOC will render this a a monospace literal
   * @param s
   * @return
   */
  public String extractCodeBlockMarker(String s) {
    String regex = "cb=(.*)";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(s);
    if (matcher.matches()) {
      return matcher.group(1);
    }
    return null;
  }

  /***
   * == The i directive (Include)
   *
   * Syntax: i=Reference
   *
   * This directive will be replaced with a
   *
   * * Block (b)
   * * Codeblock (cb)
   *
   * @param s
   * @return
   */
  public String extractIncludeMarker(String s) {
    String regex = "i=(.*)";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(s);
    if (matcher.matches()) {
      return matcher.group(1);
    }
    return null;
  }

  /***
   * == The d and dl directive (Diagram and diagram line)
   *
   * === Diagram
   *
   * Syntax: d=[myId];Title [myTitle]
   *
   * Starts a new diagram with the id "myId" and the title "myTitle".
   *
   * The diagram will not print directly, but.has to be included later
   *
   * === Diagram line
   *
   * Syntax: dl=[asciidoc]
   *
   * Will append "asciidoc" to the current diagram
   *
   * @param s
   * @return
   */
  public DiagramLine extractDiagramLine(String s) {
    String regex = "(dl?)=(.*)";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(s);
    if (matcher.matches()) {
      String directive = matcher.group(1);
      String[] data = matcher.group(2).split(";");
      if (directive.equals("d")) {
        DiagramLine diagramLine = new DiagramLine();
        diagramLine.diagramName = data[0];
        diagramLine.line = data[1];
        return diagramLine;
      } else {
        DiagramLine diagramLine = new DiagramLine();
        diagramLine.diagramName = null;
        diagramLine.line = data[0];
        return diagramLine;

      }
    }
    return null;
  }

  public EnumLine extractEnumLine(String s) {
    String regex = "enum=(.*);(.*)";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(s);
    if (matcher.matches()) {
      EnumLine line = new EnumLine();
      line.enumName = matcher.group(1);
      line.fields = matcher.group(2);
      return line;
    }
    return null;
  }

  /***
   * == Placeholders
   *
   * Syntax: $ {id}
   *
   * Please remove the space between $ and the remainder.
   *
   * Placeholder
   *
   *
   * @param s
   * @param log
   * @return
   */
  public List<String> getPlaceholders(final String s, final Log log) {
    List<String> result = new ArrayList<>();
    if (s != null && !s.isEmpty()) {
      String pattern = "\\$\\{([^\\}]+)\\}";
      Pattern p = Pattern.compile(pattern);
      Matcher m = p.matcher(s);
      while (m.find()) {
        String ph = m.group(1);
        log.info("Found placeholder " + ph);
        result.add(ph);
      }
    }
    return result;
  }


}
