package de.audiophobe;

import java.util.ArrayList;
import java.util.List;

/**
 * A Collection of Strings
 */
public class CalciferBlock extends CalciferAtom {

  public CalciferBlock() {
    super();
  }

  public CalciferBlock(String id) {
    super();
    setId(id);
  }

  private List<String> entries = new ArrayList<>();

  public boolean isEmpty() {
    return entries.isEmpty();
  }

  public void add(String s) {
    entries.add(s);
  }

  @Override
  public List<String> getContents(boolean isFragment, CalciferProject calciferProject) {
    List<String> result = new ArrayList<>();
    result.addAll(entries);
    return result;
  }

  public List<String> getEntries() {
    return entries;
  }

  public void setEntries(List<String> entries) {
    this.entries = entries;
  }

}
